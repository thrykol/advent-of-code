use itertools::Itertools;

fn main() {
  let data: Vec<u16> = std::fs::read_to_string("inputs/01.txt")
    .unwrap_or_else(|e| panic!("failed to read path: {}", e))
    .split_whitespace()
    .filter_map(|depth| depth.parse::<u16>().ok())
    .collect();

  println!("Part 1: {:?}", compute(&data, 2));
  println!("Part 2: {:?}", compute(&data, 3));
}

fn compute(data: &Vec<u16>, permutations: usize) -> u32 {
  data
    .into_iter()
    .permutations(permutations) // more expensive the longer the list
    .filter_map(|perm| {
      let perm: Vec<u16> = perm.into_iter().map(|v| *v).collect();
      if perm.iter().sum::<u16>() == 2020u16 {
        perm.into_iter().map(|e| e as u32).product1::<u32>()
      } else {
        None
      }
    })
    .next()
    .unwrap_or_default()
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn test_compute() {
    assert_eq!(compute(&vec![1721, 979, 366, 299, 675, 1456], 2), 514579);
    assert_eq!(compute(&vec![1721, 979, 366, 299, 675, 1456], 3), 241861950);
  }
}

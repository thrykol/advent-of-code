# advent-of-code

Solutions for [Advent of Code 2021](https://adventofcode.com/2021).

```shell
# Day one
cargo run --bin d01
```
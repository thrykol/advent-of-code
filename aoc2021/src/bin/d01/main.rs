use advent_of_code::DayOne;

fn main() {
  let depths: Vec<u16> = DayOne::report();

  part_one(&depths);
  part_two(&depths);
}

fn part_one(depths: &Vec<u16>) {
  let increases: Vec<()> = depths
    .iter()
    .zip(depths.iter().skip(1))
    .filter_map(|(p, c)| if c > p { Some(()) } else { None })
    .collect();

  println!("part one: {}", increases.len());
}

fn part_two(depths: &Vec<u16>) {
  let increases: Vec<()> = depths
    .iter()
    .zip(depths.iter().skip(3))
    .filter_map(|(p, c)| if c > p { Some(()) } else { None })
    .collect();

  println!("part two: {}", increases.len());
}

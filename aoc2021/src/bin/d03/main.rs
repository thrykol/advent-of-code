use nom::bytes::complete::take;
use nom::character::complete::{multispace0, one_of};
use nom::combinator::map;
use nom::error::{Error, ErrorKind};
use nom::multi::{fold_many1, many1};
use nom::{IResult, ToUsize};

fn main() {
  let input = std::fs::read_to_string("inputs/03.txt").unwrap();
  println!("p1: {}", p1(input.as_str()));
  println!("p2: {}", p2(input.as_str()));
}

fn p1(i: &str) -> isize {
  let bits = fold_many1(
    bits,
    || [0i16; 12],
    |mut acc, item| {
      item
        .into_iter()
        .enumerate()
        .for_each(|(i, x)| if x == 1 { acc[i] += 1 } else { acc[i] -= 1 });
      acc
    },
  )(i)
  .unwrap()
  .1
  .into_iter()
  .map(|i| if i >= 0 { "1" } else { "0" })
  .collect::<Vec<&'static str>>()
  .join("");

  let bits: &str = bits.as_ref();
  let fbits = many1(map(one_of::<_, _, (&str, ErrorKind)>("01"), |v| if v == '0' { "1" } else { "0" }))(bits)
    .unwrap()
    .1
    .join("");

  let gamma = isize::from_str_radix(bits, 2).unwrap();
  let epsilon = isize::from_str_radix(fbits.as_str(), 2).unwrap();

  gamma * epsilon
}

fn p2(i: &str) -> isize {
  let entries = many1(bits)(i).unwrap().1;
  let oxygen = find(entries.clone(), 1);
  let co2 = find(entries.clone(), 0);

  oxygen * co2
}

fn find(mut entries: Vec<Vec<u8>>, target_bit: u8) -> isize {
  for index in 0..12 {
    let (count, ones, zeros) = entries.into_iter().fold((0, vec![], vec![]), |(c, mut o, mut z), v| {
      if *v.get(index).unwrap() == 1 {
        o.push(v);
        (c + 1, o, z)
      } else {
        z.push(v);
        (c - 1, o, z)
      }
    });

    if target_bit == 1 && count >= 0 {
      entries = ones;
    } else if target_bit == 1 {
      entries = zeros;
    } else if count >= 0 {
      entries = zeros;
    } else {
      entries = ones;
    }

    if entries.len() == 1 {
      break;
    }
  }

  let bits = entries
    .into_iter()
    .next()
    .unwrap()
    .iter()
    .map(|v| if *v == 1 { "1" } else { "0" })
    .collect::<Vec<&str>>()
    .join("");
  let radix = isize::from_str_radix(bits.as_str(), 2).unwrap();

  radix
}

fn vec_n<C>(i: &str, count: C) -> IResult<&str, Vec<&str>>
where
  C: ToUsize,
{
  let mut input = i;
  let mut vec = vec![];
  for _ in 0..count.to_usize() {
    let (i, v) = take(1usize)(input)?;
    input = i;
    vec.push(v);
  }

  Ok((input, vec))
}

fn bits(i: &str) -> IResult<&str, Vec<u8>> {
  let (input, result) = vec_n(i, 12usize)?;
  let (input, _) = multispace0(input)?;

  let mut vec = vec![];
  for v in result {
    vec.push(
      v.parse::<u8>()
        .map_err(|_| nom::Err::Failure(Error::new(v, ErrorKind::Digit)))
        .map(|v1| (v, v1))?
        .1,
    );
  }

  Ok((input, vec))
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn test_p1() {
    let input = "010101110000
010011000110
010101000011
111100100001";
    assert_eq!(3745364, p1(input));
  }

  #[test]
  fn test_p2() {
    let input = "010101110000
010011000110
010101000011
111100100001";

    assert_eq!(5391216, p2(input));
  }
}

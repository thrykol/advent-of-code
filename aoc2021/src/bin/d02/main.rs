use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::{digit1, multispace0};
use nom::combinator::map_res;
use nom::error::{Error, ErrorKind};
use nom::multi::many1;
use nom::sequence::{terminated, tuple};
use nom::IResult;

fn main() {
  let input = std::fs::read_to_string("inputs/02.txt").unwrap();
  println!("p1: {}", p1(&input));
  println!("p2: {}", p2(&input));
}

#[derive(Debug)]
enum Direction {
  Horizontal(i16),
  Vertical(i16),
}

impl TryFrom<(String, i16)> for Direction {
  // TODO: how to use ('static str, i16) for signature
  type Error = nom::Err<Error<&'static str>>;

  fn try_from((dir, dis): (String, i16)) -> Result<Self, Self::Error> {
    match dir.as_str() {
      "forward" => Ok(Self::Horizontal(dis)),
      "down" => Ok(Self::Vertical(dis)),
      "up" => Ok(Self::Vertical(dis * -1)),
      _ => Err(nom::Err::Failure(Error::new("tbd", ErrorKind::Alpha))),
    }
  }
}

fn dir(i: &str) -> IResult<&str, Direction> {
  map_res(
    tuple((alt((tag("forward"), tag("down"), tag("up"))), multispace0, digit1)),
    |(dir, _, dis): (&str, &str, &str)| {
      dis
        .parse::<i16>()
        .map_err(|_| nom::Err::Failure(Error::new(dis, ErrorKind::Digit)))
        .and_then(|dis| Direction::try_from((dir.to_owned(), dis)))
    },
  )(i)
}

fn many1_dir(i: &str) -> Vec<Direction> {
  many1(terminated(dir, multispace0))(i).unwrap().1
}

fn p1(i: &str) -> i32 {
  let (h, v) = many1_dir(i).iter().fold((0i32, 0i32), |(h, v), direction| match direction {
    Direction::Vertical(x) => (h, v + *x as i32),
    Direction::Horizontal(x) => (h + *x as i32, v),
  });

  h * v
}

fn p2(i: &str) -> i32 {
  let directions = many1_dir(i);
  let (h, v, _) = directions.iter().fold((0i32, 0i32, 0i32), |(h, v, a), direction| match direction {
    Direction::Vertical(x) => (h, v, a + *x as i32),
    Direction::Horizontal(x) => (h + *x as i32, v + a * *x as i32, a),
  });

  h * v
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn test_p1() {
    let input = "forward 5
down 5
forward 8
up 3
down 8
forward 2";
    assert_eq!(150, p1(input));
  }

  #[test]
  fn test_p2() {
    let input = "forward 5
down 5
forward 8
up 3
down 8
forward 2";
    assert_eq!(900, p2(input));
  }
}

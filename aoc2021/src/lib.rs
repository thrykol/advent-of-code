use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub struct DayOne {
  #[structopt(short, long, default_value = "inputs/01.txt")]
  report_path: PathBuf,
}

impl DayOne {
  pub fn report() -> Vec<u16> {
    std::fs::read_to_string(DayOne::from_args().report_path)
      .unwrap_or_else(|e| panic!("failed to read path: {}", e))
      .split_whitespace()
      .filter_map(|depth| depth.parse::<u16>().ok())
      .collect()
  }
}
